# PyControl Plugin - Session History

This plugin shows a pretty-print version of session history.


## How to install

First generate package (or download from the 'downloads' section on bitbucket):

	python3 setup.py sdist --formats=zip

Then copy this folder inside the pycontrolgui/plugins folder.
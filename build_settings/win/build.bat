set WINPYDIR=C:\Users\swp\Python\WinPython-32bit-3.4.3.7\python-3.4.3
set WINPYVER=3.4.3.7
set HOME=%WINPYDIR%\..\settings
set WINPYARCH="WIN32"

set PATH=%WINPYDIR%\Lib\site-packages\PyQt4;%WINPYDIR%\;%WINPYDIR%\DLLs;%WINPYDIR%\Scripts;%WINPYDIR%\..\tools;

rem keep nbextensions in Winpython directory, rather then %APPDATA% default
set JUPYTER_DATA_DIR=%WINPYDIR%\..\settings

set PROJECTNAME="pycontrol-gui-plugin-session-log"

pip uninstall -y %PROJECTNAME%
rem pip install .
rem pip show %PROJECTNAME%

python setup.py sdist --formats=zip
# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pycontrolgui_plugin_experiment_macros.macros_handler import MacrosHandlerWindow
from pysettings import conf

class SessionTreeNode(object):
	
	def create_treenode(self, tree):
		node = super(SessionTreeNode, self).create_treenode(tree)
		tree.addPopupMenuOption('Execute macros', self.__open_macros_plugin, item=self.node, icon=conf.TIMELINE_PLUGIN_ICON)
		return node

	def __open_macros_plugin(self):
		if not hasattr(self, 'macros_plugin'):
			self.macros_plugin = MacrosHandlerWindow(self)
			self.macros_plugin.show()
			self.macros_plugin.subwindow.resize(*conf.TIMELINE_PLUGIN_WINDOW_SIZE)			
		else:
			self.macros_plugin.show()


	def remove(self):
		if hasattr(self, 'macros_plugin'): self.mainwindow.mdi_area -= self.macros_plugin
		super(SessionTreeNode, self).remove()


	@property
	def name(self): return super(SessionTreeNode, self.__class__).name.fget(self)
	@name.setter
	def name(self, value):
		super(SessionTreeNode, self.__class__).name.fset(self, value)
		if hasattr(self, 'macros_plugin'): self.macros_plugin.title = value
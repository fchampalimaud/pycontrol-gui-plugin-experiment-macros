# !/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
from pysettings import conf
import rpyc
from pycontrolgui_plugin_experiment_macros.macros_window import MacrosWindow

class ExperimentTreeNode(object):
	
	def create_treenode(self, tree):
		node = super(ExperimentTreeNode, self).create_treenode(tree)
		tree.addPopupMenuOption('Setup macros', self.__open_experiment_macros_plugin, item=self.node, icon=conf.EXPERIMENTMACROS_PLUGIN_ICON)
		
		return node

	def __open_experiment_macros_plugin(self):
		if not hasattr(self, 'experiment_macros_plugin'):
			self.experiment_macros_plugin = MacrosWindow(self)
			self.experiment_macros_plugin.show()			
			self.experiment_macros_plugin.subwindow.resize(*conf.EXPERIMENTMACROS_PLUGIN_WINDOW_SIZE)         
		else:
			self.experiment_macros_plugin.show()


	def remove(self):
		if hasattr(self, 'experiment_macros_plugin'): self.mainwindow.mdi_area -= self.experiment_macros_plugin
		super(ExperimentTreeNode, self).remove()



	@property
	def name(self): return super(ExperimentTreeNode, self.__class__).name.fget(self)
	@name.setter
	def name(self, value):
		super(ExperimentTreeNode, self.__class__).name.fset(self, value)
		if hasattr(self, 'experiment_macros_plugin'): self.experiment_macros_plugin.title = value


	def save(self, project_path, data):
		if hasattr(self, 'experiment_macros_plugin'): data = self.experiment_macros_plugin.save(project_path, data)
		data = super(ExperimentTreeNode, self).save(project_path, data)
		return data

	def load(self, experiment_path, data): 
		data = super(ExperimentTreeNode, self).load(experiment_path, data)
		
		macros = {}
		for row in data.get('macros',[]):
			state_name = row['state']
			macro_name = row['name']
			macro_code = row['code']
			macros[macro_name] = {'code': macro_code, 'state':state_name}

		self.macros = macros

		return data



		
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os


EXPERIMENTMACROS_PLUGIN_ICON = os.path.join(os.path.dirname(__file__), 'resources', 'macro.png')


EXPERIMENTMACROS_PLUGIN_WINDOW_SIZE = 800, 600

EXPERIMENTMACROS_PLUGIN_REFRESH_RATE = 1000
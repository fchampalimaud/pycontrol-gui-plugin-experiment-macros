# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging, sys, io, traceback
from pysettings import conf
from pyforms import BaseWidget
from pyforms.Controls import ControlList
from pyforms.Controls import ControlCodeEditor
from pyforms.Controls import ControlText
from pyforms.Controls import ControlButton
from PyQt4 import QtCore


logger = logging.getLogger(__name__)


class MacrosWindow(BaseWidget):
	""" ProjectWindow represents the project entity as a GUI window"""

	def __init__(self, experiment):
		BaseWidget.__init__(self, 'Macros')

		self.experiment  = experiment

		self._macros   = ControlList('Macros', plusFunction=self.__add_macro, minusFunction=self.__remove_macro)
		self._code     = ControlCodeEditor('Script code')
		self._script   = ControlText('Script name')
		self._state    = ControlText('State name')
		self._edit_btn 		= ControlButton('Edit')
		self._save_btn 		= ControlButton('Save')
		self._cancel_btn 	= ControlButton('Cancel')

		
		self._formset = [
			'_macros',
			'_edit_btn',
			('_state','_script'),
			'_code',
			(' ','_cancel_btn','_save_btn'),
		]

		self._macros.horizontalHeaders 	= ['STATE','SCRIPT NAME']
		self._macros.selectEntireRow = True
		self._macros.readOnly = True
		self._cancel_btn.value = self.__cancel
		self._save_btn.value = self.__save
		self._edit_btn.value = self.__edit

		self.__cancel()

		if not hasattr(experiment, 'macros'): 
			experiment.macros = {}
		else:
			for key, value in self.experiment.macros.items():
				self._macros += (value['state'], key)


				
	def __edit(self):
		row = self._macros.value[self._macros.mouseSelectedRowIndex]
		name = row[1]
		data = self.experiment.macros[name]

		self._current_macro = self._macros.mouseSelectedRowIndex
		self._script.value 	= name
		self._state.value 	= data['state']
		self._code.value 	= data['code']

		self.__add_macro()

	def __save(self):
		data = {'code': self._code.value, 'state':self._state.value}
		self.experiment.macros[self._script.value] = data

		if hasattr(self, '_current_macro'):
			rows = self._macros.value
			rows[self._current_macro] = (self._state.value, self._script.value)
			self._macros.value = rows
			del self._current_macro
		else:
			self._macros += (self._state.value, self._script.value)
		
		self.__cancel()
		



	def __cancel(self):
		self._script.hide()
		self._state.hide()
		self._save_btn.hide()
		self._cancel_btn.hide()
		self._code.hide()

		self._script.value 	= ''
		self._state.value 	= ''
		self._code.value 	= ''

	def __add_macro(self):
		self._script.show()
		self._state.show()
		self._save_btn.show()
		self._cancel_btn.show()
		self._code.show()

	def __remove_macro(self):
		row = self._macros.value[self._macros.mouseSelectedRowIndex]
		del self.experiment.macros[row[1]]
		self._macros -= -1
		
		
	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called


	def beforeClose(self):	return False

	@property
	def mainwindow(self): return self.experiment.mainwindow	

	
	def save(self, project_path, data):
		data['macros'] = []
		for row in self._macros.value:
			state_name = row[0]
			macro_name = row[1]

			macro = {'name': macro_name, 'state': self.experiment.macros[macro_name]['state'], 'code':self.experiment.macros[macro_name]['code'] }

			data['macros'].append(macro)


		return data

	def load(self, experiment_path, data):
		macros = []
		for row in data.get('macros',[]):
			state_name = row['state']
			macro_name = row['name']
			macro_code = row['code']

			macros.append( (state_name, macro_name) )
			self.experiment.macros[macro_name] = {'code': macro_code, 'state':state_name}

		self._macros.value = macros
		return data
#!/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolgui_plugin_timeline.trials_plot"""



from PyQt4.QtCore import QTimer
from pyforms import BaseWidget
from pycontrolapi.models.board.com.state_entry import StateEntry
from pycontrolapi.exceptions.run_setup import RunSetupError
from pysettings import conf

import logging; logger = logging.getLogger(__name__)



class MacrosHandlerWindow(BaseWidget):
	""" Show all boxes live state for an experiment"""
	
	def __init__(self, session):
		"""
		:param session: session reference
		:type session: pycontrolgui.windows.detail.entities.session_window.SessionWindow
		"""
		BaseWidget.__init__(self, session.name)
		self.session = session
		

		self._formset = ['waiting for events...']
		
		self._history_index = 0

		self.macros = {}

		self._locals = locals()
		self._globals = globals()

		self._timer = QTimer()
		self._timer.timeout.connect(self.read_message_queue)

		self.session.mainwindow.model.open_terminal_plugin()


	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called

		if hasattr(self.session.setup.experiment, 'macros'):
			self.macros = {}
			states = self.session.setup.board_task.states

			self._history_index = len(self.session.messages_history)

			new_states = {}
			for k, v in states.items():
				new_states[v] = k
			
			for macro_name, macro in self.session.setup.experiment.macros.items():
				state_id = new_states[macro['state']]
				if state_id not in self.macros.keys():
					self.macros[state_id] = []

				self.macros[state_id].append( macro['code'] )

			


		print('macros', self.macros)

		self._timer.start(conf.TIMELINE_PLUGIN_REFRESH_RATE)

	def hide(self): self._timer.stop()

	def beforeClose(self):
		self._timer.stop()
		return False

	
	def read_message_queue(self):
		""" Update board queue and retrieve most recent messages """
		
		try:
			recent_history = self.session.messages_history[self._history_index:]
			states 		   = self.session.setup.board_task.states

			for message in recent_history:
				if isinstance(message, StateEntry):
					for code in self.macros.get(message._state_id, []):
						self._locals['session'] = self.session

						self.session.mainwindow.model.terminal_plugin.execute_script(code, self._locals, self._globals)
				self._history_index += 1

		except RunSetupError as err:
			logger.error(str(err), exc_info=True)
			self._timer.stop()

	@property
	def mainwindow(self): return self.session.mainwindow
	

	@property
	def title(self): return BaseWidget.title.fget(self)
	@title.setter
	def title(self, value):
		title = 'Macros handler: {0}'.format(value)
		BaseWidget.title.fset(self, title)
